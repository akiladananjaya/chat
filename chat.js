var admin = require("firebase-admin");
const { exec } = require("child_process");

var name = 'akila';
var os = 'wsl';


var separator = '|#|';

admin.initializeApp({
  credential: admin.credential.cert(
   
),
  databaseURL: "https://project-2342.firebaseio.com/"
});

var db = admin.database();
var msgRef = db.ref("messages/");

msgRef.on('child_changed', async (snapshot, prevChildKey) => {
  try {
    const newMessage = snapshot.val();
    if (newMessage && (newMessage.split([separator])[0] === name)) {
      return;
    }
    console.log(newMessage.split([separator])[1]);
    if (os == 'wsl') {
      exec("powershell.exe -File notify.ps1", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
      });
    }
  } catch (error) {
    console.log(error)
  }
});

var stdin = process.openStdin();

stdin.addListener("data", function(d) {
    var msg = d.toString().trim(); 
    msgRef.update({
      latestMessage: `${name}${separator}${msg}` 
    });
});
